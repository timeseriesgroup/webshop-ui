FROM nginx:1.13.0-alpine

COPY ./dist/ /usr/share/nginx/html/
COPY ./static/ /usr/share/nginx/html/static/
COPY nginx.conf /etc/nginx/conf.d/default.conf