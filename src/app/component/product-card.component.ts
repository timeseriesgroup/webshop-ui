import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";

import {Subscription} from "rxjs/Subscription";

import {Product} from "../domain/product";
import {Basket} from "../domain/basket";
import {BasketService} from "../service/basket.service";

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit, OnDestroy {
  @Input()
  product: Product;

  countControl = new FormControl();

  private subscription: Subscription;
  private count: number = 0;

  constructor(private basketService: BasketService) {}

  ngOnInit(): void {
    this.subscription = this.basketService.basketSubject
        .subscribe(basket => this.setCountFromBasket(basket));
    this.countControl.valueChanges
        .debounceTime(250)
        .subscribe(newValue => {
          if (newValue <= 0) {
            this.basketService.removeProduct(this.product.id);
          } else {
            this.basketService.setProductCount(this.product.id, newValue);
          }
        });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  showImage: boolean = true;

  get imageUrl(): string {
    return process.env.API_URL + "/" + this.product.image;
  }

  imageLoadError(event: object): void {
    this.showImage = false;
  }

  private setCountFromBasket(basket: Basket) {
    let entry = basket.entries[this.product.id];
    if (entry) {
      this.count = entry.count;
    } else {
      this.count = 0;
    }
  }
}
