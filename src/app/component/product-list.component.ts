import {Component, OnInit} from "@angular/core";
import {ProductService} from "../service/product.service";
import {Product} from "../domain/product";

@Component({
	selector: 'product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
	products: Product[];

	constructor(private productService: ProductService) { }

	ngOnInit(): void {
		this.productService.findAllProducts()
				.then(products => this.products = products);
	}
}
