import {Component, OnDestroy, OnInit} from "@angular/core";
import {BasketService} from "../service/basket.service";
import {Subscription} from "rxjs/Subscription";
import {Basket} from "../domain/basket";

@Component({
  selector: 'basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private basket: Basket;

  constructor(private basketService: BasketService) {}

  ngOnInit(): void {
    this.subscription = this.basketService.basketSubject
        .subscribe(basket => this.basket = basket);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  // TODO: debounce
  setCount(productId: string, event: Event): void {
    let value = event.target['value'];
    if (value <= 0) {
      this.basketService.removeProduct(productId);
    }else {
      this.basketService.setProductCount(productId, value);
    }
  }
}
