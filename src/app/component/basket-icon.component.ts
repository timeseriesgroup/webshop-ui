import {Component, OnDestroy, OnInit} from "@angular/core";
import {Basket} from "../domain/basket";
import {Subscription} from "rxjs/Subscription";
import {BasketService} from "../service/basket.service";

@Component({
  selector: 'basket-icon',
  templateUrl: './basket-icon.component.html',
  styleUrls: ['./basket-icon.component.scss']
})
export class BasketIconComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  private _basket: Basket;
  count: number;

  constructor(private basketService: BasketService) {}

  ngOnInit(): void {
    this.subscription = this.basketService.basketSubject
        .subscribe(basket => this.basket = basket);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  get basket(): Basket {
    return this._basket;
  }

  set basket(basket: Basket) {
    let count = 0;
    for (let key of Object.keys(basket.entries)) {
      count += basket.entries[key].count;
    }
    this._basket = basket;
    this.count = count;
  }
}
