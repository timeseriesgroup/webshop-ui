import {Component, ViewEncapsulation} from "@angular/core";

@Component({
	selector: 'webshop-app',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class WebshopApp {

}
