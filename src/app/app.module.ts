import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";

import {AppRoutingModule} from "./app-routing.module";
import {ProductListComponent} from "./component/product-list.component";
import {ProductCardComponent} from "./component/product-card.component";
import {ProductService} from "./service/product.service";
import {WebshopApp} from "./component/app.component";
import {MapToEntries} from "./pipe/map-to-entries.pipe";
import {BasketComponent} from "./component/basket.component";
import {BasketService} from "./service/basket.service";
import {BasketIconComponent} from "./component/basket-icon.component";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProductCardComponent,
    ProductListComponent,
    BasketComponent,
    BasketIconComponent,
    MapToEntries,
    WebshopApp
  ],
  providers: [
    ProductService,
    BasketService
  ],
  bootstrap: [WebshopApp]
})
export class AppModule {
}
