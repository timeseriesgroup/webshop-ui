import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ProductListComponent} from "./component/product-list.component";
import {BasketComponent} from "./component/basket.component";

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  { path: 'products', component: ProductListComponent },
  { path: 'basket', component: BasketComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
