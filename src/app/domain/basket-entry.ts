import {Product} from "./product";

export class BasketEntry {
  product: Product;
  count: number;
  subTotal: number;
}
