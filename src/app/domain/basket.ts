import {BasketEntry} from "./basket-entry";

export class Basket {
  id: string;
  entries: { [key: string]: BasketEntry; };
  total: number;
}
