import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

import "rxjs/add/operator/toPromise";

import {Product} from "../domain/product";

@Injectable()
export class ProductService {
  private apiUrl: string;

  constructor(private http: Http) {
    this.apiUrl = process.env.API_URL;
  }

  findAllProducts(): Promise<Product[]> {
    const url = `${this.apiUrl}/api/1.0/product`;
    return this.http.get(url)
        .toPromise()
        .then(response => {
          response.headers.get("node-uuid") && console.log("Products from node:", response.headers.get("node-uuid"));
          return response.json() as Product[];
        })
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // TODO: for demo purposes only
    return Promise.reject(error.message || error);
  }
}
