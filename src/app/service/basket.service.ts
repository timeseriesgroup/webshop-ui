import {Injectable, OnInit} from "@angular/core";
import {Headers, Http, RequestOptionsArgs} from "@angular/http";

import "rxjs/add/operator/toPromise";
import {Basket} from "../domain/basket";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class BasketService {

  basketSubject: ReplaySubject<Basket> = new ReplaySubject(1);

  private args: RequestOptionsArgs;
  private apiUrl: string;
  private basket: Basket;

  constructor(private http: Http) {
    this.apiUrl = process.env.API_URL;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.args = {
      headers: headers
    };
    this.refreshBasket();
  }

  refreshBasket(): void {
    let id: string = localStorage.getItem('basket');
    if (id) {
      this.findBasket(id)
          .then(basket => this.setBasket(basket))
          .catch(reason => {
            if (reason.status == 404) {
              console.warn(`Could not find basket ${id}.`);
              this.createBasket()
                  .then(basket => this.setBasket(basket))
            }
            // TODO: Deal with exceptional situation
          })
    } else {
      this.createBasket()
          .then(basket => this.setBasket(basket));
    }
  }

  setProductCount(productId: string, count: number): void {
    if(count < 1){
      throw new Error(`Can not set product count of ${productId} to ${count}.`);
    }
    const url = `${this.apiUrl}/api/1.0/basket/${this.basket.id}/product/${productId}`;
    this.http.put(url, count, this.args)
        .toPromise()
        .then(response => {
          response.headers.get("node-uuid") && console.log("Basket update on node:", response.headers.get("node-uuid"));
          this.refreshBasket()
        })
        .catch(this.handleError);
  }

  removeProduct(productId: string): void {
    const url = `${this.apiUrl}/api/1.0/basket/${this.basket.id}/product/${productId}`;
    this.http.delete(url, this.args)
        .toPromise()
        .then(response => {
          response.headers.get("node-uuid") && console.log("Basket update on node:", response.headers.get("node-uuid"));
          this.refreshBasket()
        })
        .catch(this.handleError);
  }

  private findBasket(id: string): Promise<Basket> {
    const url = `${this.apiUrl}/api/1.0/basket/${id}`;
    return this.http.get(url)
        .toPromise()
        .then(response => {
          response.headers.get("node-uuid") && console.log("Basket from node:", response.headers.get("node-uuid"));
          return response.json() as Basket
        })
        .catch(this.handleError);
  }

  private createBasket(): Promise<Basket> {
    const url = `${this.apiUrl}/api/1.0/basket`;
    return this.http.post(url, null)
        .toPromise()
        .then(response => response.json() as Basket)
        .catch(this.handleError);
  }

  private setBasket(basket: Basket) {
    localStorage.setItem('basket', basket.id);
    this.basket = basket;
    this.basketSubject.next(basket);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // TODO: for demo purposes only
    return Promise.reject(error.message || error);
  }
}
