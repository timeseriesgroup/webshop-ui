import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: "mapToEntries"})
export class MapToEntries implements PipeTransform {
	transform(value: object): any {
		return Object.keys(value)
				.map(key => new Entry(key, value[key]));
	}
}

export class Entry{
	key: string;
	value: any;

	constructor(key: string, value:any){
		this.key = key;
		this.value = value;
	}
}